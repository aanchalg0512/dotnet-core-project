using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NetCoreProject.Data;
using NetCoreProject.Models;

namespace NetCoreProject.Pages.Users
{
    [Authorize(Roles = "Admin")]
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _db;

        public IndexModel(ApplicationDbContext db)
        {
            _db = db;
        }

        [BindProperty]
        public List<ApplicationUser> ApplicationUserList { get; set; }
        public async Task<IActionResult> OnGet()
        {
            ApplicationUserList = await _db.ApplicationUser.ToListAsync();
            return Page();
        }
        public async Task<IActionResult> OnPostDelete(string id)
        {
            var applicationUser = await _db.ApplicationUser.FindAsync(id);
            if (applicationUser == null)
            {
                return NotFound();

            }
            _db.ApplicationUser.Remove(applicationUser);
            await _db.SaveChangesAsync();

            return RedirectToPage("Index");
        }

    }
}
