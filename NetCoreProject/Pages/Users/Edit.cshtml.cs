using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NetCoreProject.Data;
using NetCoreProject.Models;

namespace NetCoreProject.Pages.Users
{
    public class EditModel : PageModel
    {
        private ApplicationDbContext _db;

        public EditModel(ApplicationDbContext db)
        {
            _db = db;
        }

        [BindProperty]
        public ApplicationUser ApplicationUser { get; set; }
        public async Task OnGet(string id)
        {
            ApplicationUser = await _db.ApplicationUser.FindAsync(id);
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                var user = await _db.ApplicationUser.FindAsync(ApplicationUser.Id);
                user.Name = ApplicationUser.Name;
                user.Email= ApplicationUser.Email;
                user.PhoneNumber = ApplicationUser.PhoneNumber;
                user.Address = ApplicationUser.Address;
                user.City = ApplicationUser.City;
                user.PostalCode = ApplicationUser.PostalCode;

                await _db.SaveChangesAsync();
                return RedirectToPage("Index");

            }
            return RedirectToPage();
        }
    }
}
